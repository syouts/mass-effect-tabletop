Movement Rates:
---

The following are the maximum movement rates for any particular orientation and type of movement.

Characters attempting to move without being detected cannot move at a rate faster than a "jog."

Standing, Walk: 4m/2h per round

Standing, Jog: 8m/4h per round

Standing, Sprint: 10m/5h per round

Crouched, Walk: 2m/1h per round

Crouched, Fast Walk: 4m/2h per round

Prone, Crawling: 2m/1h per round
