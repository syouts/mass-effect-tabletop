Erik Talmor
Level 10 Human Soldier/Tech
Cover: 34%
Shield: 37.5


[COMBAT SKILLS] 50

GENERAL:
Light Weapons: 10

SOLDIER:
Firestorm: 10
Heavy Armor: 10

TECH:
Medicine: 10
Sabotage: 10


[NON-COMBAT SKILS] 40
Genetics: 10
Arts, History, and Culture: 10
Navigation Training: 10
Performance Operation: 10


[INVENTORY]
Gladiator Heavy Armor 24%  Cover, 37.5 Shield HP
Locust SMG
20 x Thermal Clips
10 x Medi-Gel
5x Antibiotics

[HISTORY]
Grad School dropout, joined mercs
