Combat Concepts:
---

The primary concepts of combat are combat style, damage, cover, and the combat round.

Every player character typically chooses a primary and secondary combat style. The primary style is the most-emphasized, with the chosen abilities in this style able to progress at a rate equivalent to the number of levels the character has. The secondary style is less-emphasized and abilities chosen in this style progress at a rate equivalent to half of the character's levels. This system is similar to picking a set of class skills and cross-class skills for combat, with the unselected combat style completely unavailable.

If a player wishes to focus on only one combat style, the character receives a 10% circumstance bonus to all skill checks involving their chosen combat style.

The cover system is fairly simple, and cover actually functions more like a damage reduction system in other games. A character completely behind cover receives 100% cover, which makes them impossible to damage by armaments incapable of either circumventing or obliterating their cover. A character with 50% cover receives half the amount of damage they would if they were out in the open. And so forth.

Armor affects cover -- light armor grants a continuous 10% bonus to cover, heavy armor grants a 20% bonus, and juggernaut armor grants a 30% bonus. A character with additional combat skill ranks in armor use gains a 1% bonus to cover for every rank taken. High-quality armors can give bonuses to cover depending on their quality.

Shields also affect cover. A character is considered to have 100% cover for the duration its shields are active. Shields have their own hitpoint pool, which is affected by their power level. When the shields die, their influence on the character's cover is removed.

Damage affects character hitpoints. The base level of damage a weapon is capable of dealing varies depending on the type of weapon and ammunition employed. A pistol normally capable of dealing 25 damage deals only 12.5 damage if the target has 50% cover. And so forth. Ammunition upgrades can affect the type of damage dealt, as can weapon caliber and whatnot.

The combat round is similar to d20, but more streamlined. The combat round is divided into three segments; the free segment, the move segment, and the action segment. The free segment governs things like drawing weapons or crouching behind a crate. The move segment governs character movement in the combat grid. The action segment allows characters to make use of one of their combat skills or perform a standard attack -- firing a weapon, punching someone in the face, etc. The order of character actions is determined by initiative, which is a d100 roll, modified by character level.



A character may make an attack of opportunity against a target that moves into their line of sight. Attacks of opportunity grant the target a circumstantial 50% bonus to cover to reflect their movement. This bonus stacks with armor and such.