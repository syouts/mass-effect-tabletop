Soldier Skills:
---

Assault Weapons:

Type: Passive

Grants proficiency with assault rifles, machine guns, and sniper rifles.
Each rank provides a 1% bonus to accuracy for hexagons outside of the weapon's ideal firing range.
Each rank reduces the target's cover by 1% during the current attack, unless the target has full cover.

---

Heavy Weapons:

Type: Passive

Grants proficiency with grenade launchers, rocket launchers, and miniguns.
Each rank provides a 1% bonus to accuracy for hexagons outside of the weapon's ideal firing range.
Each rank reduces the target's cover by 1% during the current attack, unless the target has full cover.

---

Heavy Armor:

Type: Passive

Grants proficiency with tactical armors and heavy armors.
Each rank provides a 1% bonus to cover from armor.

---

Concussive Round:

Type: Active
AP Cost: 100

This ability allows the soldier to fire a high-impact shot that is capable of doing splash damage in a radius.

The radius of damage increases by one meter for every ten ranks in the skill. At first level, the radius covers a cluster of seven hexagons (3 meters in diameter).

---

Penetrative Shot:

Type: Active
AP Cost: 100

This ability allows the soldier to fire a high-velocity, targetted shot that is capable of penetrating cover.

Each rank in this skill allows the penetrative shot to ignore 1% of the enemy's cover -- whether from armor, range, or environment -- up to 30%. Each rank in this skill reduces its action point cost by 1%.

---

Juggernaut:

Type: Active
AP Cost: 100

The soldier's armor is boosted by 1% for every rank in the Juggernaut skill, up to 30%. Every five ranks beyond the initial rank add another round to the duration.
