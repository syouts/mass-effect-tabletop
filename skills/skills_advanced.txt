Soldier:
---

Shock Trooper
Fireteam Leader
Assault Specialist

N7 Marine
Turian Commando
Geth Juggernaut
Quarian Marine
Batarian Raider

Technologist:
---

Hacker
Saboteur
Combat Medic

Quarian Machinist
Geth Controller
STG Splicer

Bioticist:
---

Force Adept
Stasis Adept
Field Adept

Asari Justicar
Asari Dominator
N7 Fury
Krogan Shaman

Soldier/Technologist:
---

Operative
Sniper
Infiltrator

Cerberus Assassin
N7 Shadow
Quarian Sharpshooter

Soldier/Bioticist:
---

Vanguard
Force Brawler
Skirmisher

Asari Commando
Drell Assassin
Krogan Battlemaster

Technologist/Bioticist:
---

Sentinel
