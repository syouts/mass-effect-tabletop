Gameplay Concepts:
---

The primary concepts of this system are level, combat skills, non-combat skills, and injuries.

Combat skills are those proficiencies or abilities that enable characters to succeed in battle. Some combat skills modify the accuracy and damage of firearms while others, such as biotics, take the form of abilities a character can use against a single or multiple targets. The number of ranks in a particular skill affects its power.

Non-combat skills encompass crafting/research skills, "action" skills used for non-combat actions taken, and social skills. These skills work with a DnD-style DC system.

Level governs the maximum number of points a character can have in any given combat or non-combat skill. There is no conception of hitpoints, feats, or similar; level and associated skill ranks just make the character incrementally better at what they've been trained to do.

The hitpoint system functions a bit differently than something like d20. Characters all have 100 HP, unless otherwise noted (Krogan have 200, for example). Different HP thresholds dictate injuries. A character with 76%-100% health is considered to have a flesh wound. A character with 51%-75% health is considered to be injured. A character with 26%-50% health is considered to be severely injured. A character with 1%-25% health is considered mortally wounded. Injured characters receive a 25% reduction in their skills for the duration of their injury. Severely-injured characters -- 50%. Mortally wounded characters -- 75%. A character with 0% of their hitpoints is considered dead.