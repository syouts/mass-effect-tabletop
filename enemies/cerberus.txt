Cerberus Shock Troop:
---

Level: 10
HP: 100
Shield: 50
Armor: 24
Main Attack: Burst Assault Rifle (75dmg, 6 shot, 4h)
Secondary Attack: Frag Grenade 2x (25dmg, 2h radius, 5h)
Skills: Juggernaut

Cerberus Hurricane:
---

Level: 10
HP: 100
Shield: 50
Armor: 30
Main Attack: Burst Assault Rifle (75dmg, 6 shot, 4h)
Secondary Attack: Heavy Grenade Launcher (50dmg, 3 shot, 2h radius, 5h)
Skills: Storm (3h)

Cerberus Wraith:
---

Level: 10
HP: 100
Shield: 50
Armor: 16
Main Attack: Single-Shot Sniper Rifle (37.5dmg, 12h)
Secondary Attack: Frag Grenade 1x (25dmg, 2h radius, 5h)
Skills: Tactical Cloak (1 round cooldown)

Anton Fedorov:
---

Level: 15
HP: 100
Shield: 100
Armor: 70
Main Attack: Widow Single-Shot Sniper Rifle (80dmg, 12h)
Secondary Attack: Smoke Grenade 3x (0dmg, 4h radius, 5h), Frag Grenade 2x (25dmg, 4h radius, 5h)
Skills: Tactical Cloak (1 round cooldown, 1 free action), Drone (AR - 25dm, 6h, 75hp, 7 round duration)